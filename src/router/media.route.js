const Router = require('koa-router')

const { get_video, get_music } = require('../controller/media.controller')

const router = new Router({ prefix: '/api' })

// // 注册接口
router.get('/video', get_video)

// // 登录接口
router.get('/music', get_music)

module.exports = router