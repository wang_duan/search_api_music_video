const APP_PORT = 9000

const app = require('./src/app')

app.listen(APP_PORT, () => {
  console.log(`server is running on http://localhost:${APP_PORT}`)
})